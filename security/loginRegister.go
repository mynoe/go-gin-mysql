package security

import (
	"GinWeb/common"
	"GinWeb/db"
	"GinWeb/model"
	"github.com/gin-gonic/gin"
	"net/http"
	// "time"
	"fmt"
)

func Login(c *gin.Context) {
	
	defer common.Recover(c)

	loginUser := new(model.User)
	loginUser.UserName=c.Query("username")
	loginUser.PassWord=c.Query("password")

	// JSON格式传参
	
	// if err := c.ShouldBindJSON(&loginUser); err != nil {
	// 	c.JSON(http.StatusOK, common.Error(http.StatusBadRequest, err.Error()))
	// 	return
	// }
	

	LoginCheck(loginUser)

	token, err := generateToken(loginUser)
	if err != nil {
		c.JSON(http.StatusOK, common.Error(http.StatusInternalServerError, err.Error()))
		return
	}
	c.JSON(http.StatusOK, common.AuthSuccess(token))
}

func Register(c *gin.Context) {
	defer common.Recover(c)

	registerUser := new(model.User)
	if err := c.ShouldBindJSON(registerUser); err != nil {
		c.JSON(http.StatusOK, common.Error(http.StatusBadRequest, err.Error()))
		return
	}

	user := new(model.User)
	connect := db.Connect()
	connect.Where("id = ?", registerUser.Id).First(&user)
	if user.Id == registerUser.Id && user.PassWord != registerUser.PassWord {
		//update password and name
		user.PassWord = registerUser.PassWord
		user.UserName = registerUser.UserName
		connect.Model(user).Updates(user)
	} else {
		common.PanicError("The user already exists.")
	}

	// else if !user.Id {
	// 	//create user
	// 	registerUser.CreateTime = time.Now()
	// 	registerUser.CreateUser = registerUser.Id
	// 	registerUser.UpdateTime = time.Now()
	// 	registerUser.UpdateUser = registerUser.Id
	// 	registerUser.Status = 1
	// 	connect.Create(registerUser)
	// } 
	c.JSON(http.StatusOK, common.Info())
}

func JWTAuth(c *gin.Context) {
	authToken := c.GetHeader("Authorization")
	if authToken == "" {
		c.JSON(http.StatusOK, common.AuthError())
		c.Abort()
		return
	}

	j := NewJWT()
	claims, err := j.ParseToken(authToken)

	if err != nil {
		if err == TokenExpired {
			c.JSON(http.StatusOK, common.Error(http.StatusUnauthorized, "Authorization expired"))
			c.Abort()
			return
		}
		c.JSON(http.StatusOK, common.Error(http.StatusUnauthorized, err.Error()))
		c.Abort()
		return
	}

	c.Set("claims", claims)
}

func LoginCheck(loginUser *model.User) {

	user := new(model.User)

	fmt.Println("loginUser----0",loginUser)
	connect := db.Connect()
	connect.Where("username = ?", loginUser.UserName).Find(&user)

	if loginUser.UserName == "" {
		common.PanicError("the user does not exist")
	}
	if loginUser.PassWord != loginUser.PassWord {
		common.PanicError("incorrect account or password")
	}
}
