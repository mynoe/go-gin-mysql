package model

import (
	_ "github.com/jinzhu/gorm"
	"time"
)

type User struct {
	Id         int       `json:"Id"`
	UserName    string    `json:"UserName"`
	PassWord   string    `json:"PassWord"`
	NickName       string    `json:"nickName"`
	CompanyName  string  `json:"CompanyName"`
	Logo		string 	 `json:"Logo"`
	CreateTime time.Time `json:"createTime"`
	CreateUser string    `json:"createUser"`
	UpdateTime time.Time `json:"updateTime"`
	UpdateUser string    `json:"updateUser"`
	Status     int       `json:"status"`
}

// 设置User的表名为`user`,不设置为users
func (User) TableName() string {
	return "user"
}
